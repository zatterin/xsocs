Tutorials
=========


.. _video-tutorials:

Video tutorials
+++++++++++++++

`Gilbert Chahine <https://www.youtube.com/channel/UCOqrRlLklaJb40DZ4NM_-Bg>`_ (ESRF) has made some great video tutorials:

`Part 1: Installation <https://www.youtube.com/watch?v=vgrYEnETxTA>`_

`Part 2: Starting XSOCS <https://www.youtube.com/watch?v=XHKrHBEfMk8>`_

`Part 3: Input parameters <https://www.youtube.com/watch?v=s0y3RmfaX14>`_

`Part 4: Merging into hdf5 <https://www.youtube.com/watch?v=vJPa5TeXajo>`_

`Part 5: Intensity workspace <https://www.youtube.com/watch?v=wSXkyu-cEAA>`_

`Part 6: Conversion to Q space <https://www.youtube.com/watch?v=wKi1IFQfdg8>`_

`Part 7: Q workspace isosurface plot <https://www.youtube.com/watch?v=2hf1QwwtjbA>`_

`Part 8: Q workspace cutting plane <https://www.youtube.com/watch?v=nsBWSPJJVBE>`_

`Part 9: Q workspace ROI selection <https://www.youtube.com/watch?v=YMRk1EOzQPw>`_

`Part 10: Q workspace Bragg peak fitting or COM <https://www.youtube.com/watch?v=eShYxO1ZF10>`_

`Part 11: Results workspace <https://www.youtube.com/watch?v=JbO9U7d-hPs>`_

.. note::

  Since video tutorials were made, the graphical user interface has changed.
  Yet the procedure remains valid.
